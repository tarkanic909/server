package main

type ErrorResponse struct {
	Error string `json:"error"`
}

type Versions struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

type Author struct {
	Name      string `json:"name"`
	AuthorKey string `json:"authorKey"`
}

type AuthorKeys struct {
	Authors []AuthorKey `json:"authors"`
}

type AuthorKey struct {
	Key string `json:"key"`
}
