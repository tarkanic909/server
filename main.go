package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

var version string
var commit string
var buildTime string

func checkHttpMethod(w http.ResponseWriter, r http.Request) {

	if r.Method != http.MethodGet {
		res, err := json.Marshal(ErrorResponse{Error: "Unsupported method"})
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		w.Write(res)
		return
	}
}

func getHello(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got /hello request\n")
	fmt.Fprintf(w, "Hello, HTTP!\n")
}

func getVersion(w http.ResponseWriter, r *http.Request) {
	checkHttpMethod(w, *r)
	var versions = Versions{version, commit, buildTime}
	res, err := json.Marshal(versions)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write(res)
}

func getAuthorsByBookId(w http.ResponseWriter, r *http.Request) {
	checkHttpMethod(w, *r)
	bookId := r.URL.Query().Get("book")
	if bookId == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var authorKeys AuthorKeys
	var authorArr []Author

	err := getAuthorsKey(bookId, &authorKeys)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for _, item := range authorKeys.Authors {
		var author Author
		getAuthorByKey(item.Key, &author)
		authorArr = append(authorArr, Author{author.Name, strings.Trim(item.Key, "/authors/")})
	}

	res, err := json.Marshal(authorArr)
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(res)
}

func getAuthorsKey(bookId string, authorKeys *AuthorKeys) error {
	res, err := http.Get("https://openlibrary.org/isbn/" + bookId + ".json")
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)
	json.Unmarshal(body, authorKeys)
	return nil
}

func getAuthorByKey(key string, authors *Author) error {
	res, err := http.Get("https://openlibrary.org" + key + ".json")
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)
	json.Unmarshal(body, authors)
	return nil

}

func main() {
	apiUrl := "/api/v1"

	http.HandleFunc("/", getHello)
	http.HandleFunc(apiUrl+"/version", getVersion)
	http.HandleFunc(apiUrl+"/authors", getAuthorsByBookId)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
