#!/bin/bash

version="1.0.0"
commit=$(git rev-parse HEAD)
time=$(date --rfc-3339=ns)
go build -ldflags "-X main.version=version -X main.commit=$commit -X 'main.buildTime=$time'"