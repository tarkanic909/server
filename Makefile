version="1.0.0"
commit=$(shell git rev-parse HEAD)
time=$(shell date --rfc-3339=ns)

build:
	go build -ldflags "-X main.version=version -X main.commit=$commit -X 'main.buildTime=$time'"

run:
	go build
	./server

clean:
	go clean
	rm server